# Perfect JWM (Joe's WIndows Manager) Config Files



## Why JWM?

Lightweigth. That's why! It only consume 350MB in my laptop even with all rings and bells. Second reason is, it's simple. Even tho you can add conky or picom to make it look beautiful.

## How to?

First, install required apps.<br />
If you using arch based distro, just:<br />
```Command
sudo pacman -S jwm
```

Then copy .jwmrc and .jwm folder into your home directory

## Install additional apps

Again, if you're arch user, add this to your pacman.conf<br />
```Command
[universe]
SigLevel = Never
Server = https://universe.artixlinux.org/$arch
Server = https://mirror1.artixlinux.org/universe/$arch
Server = https://mirror.pascalpuffke.de/artix-universe/$arch
Server = https://artixlinux.qontinuum.space:4443/universe/os/$arch
Server = https://mirror.alphvino.com/artix-universe/$arch
```

Then update:
```Command
sudo pacman -Sy
```

Install yaourt:
```Command
sudo pacman -S yaourt
```

Install additional apps.<br />
These apps optional, but will strengthen the DE
```Command
yaourt -S xfce4-notifyd picom xfce4-terminal mate-system-monitor eject-applet pa-applet-git gmrun pamixer gksu thunar scrot i3lock ungoogled-chromium gsimplecal reversal-icon-theme-git
```

Last, if you use picom, just copy picom.conf into .config folder. Enjoy!
